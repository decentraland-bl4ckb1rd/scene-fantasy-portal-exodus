const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)

const fenceStonePillarTall_01 = new Entity()
fenceStonePillarTall_01.setParent(scene)
const gltfShape = new GLTFShape('models/FenceStonePillarTall_01/FenceStonePillarTall_01.glb')
fenceStonePillarTall_01.addComponentOrReplace(gltfShape)
const transform_2 = new Transform({
  position: new Vector3(24, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceStonePillarTall_01.addComponentOrReplace(transform_2)
engine.addEntity(fenceStonePillarTall_01)

const fenceStonePillarTall_01_2 = new Entity()
fenceStonePillarTall_01_2.setParent(scene)
fenceStonePillarTall_01_2.addComponentOrReplace(gltfShape)
const transform_3 = new Transform({
  position: new Vector3(8, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceStonePillarTall_01_2.addComponentOrReplace(transform_3)
engine.addEntity(fenceStonePillarTall_01_2)

const fenceStonePillarTall_01_3 = new Entity()
fenceStonePillarTall_01_3.setParent(scene)
fenceStonePillarTall_01_3.addComponentOrReplace(gltfShape)
const transform_4 = new Transform({
  position: new Vector3(24, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceStonePillarTall_01_3.addComponentOrReplace(transform_4)
engine.addEntity(fenceStonePillarTall_01_3)

const fenceStonePillarTall_01_4 = new Entity()
fenceStonePillarTall_01_4.setParent(scene)
fenceStonePillarTall_01_4.addComponentOrReplace(gltfShape)
const transform_5 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceStonePillarTall_01_4.addComponentOrReplace(transform_5)
engine.addEntity(fenceStonePillarTall_01_4)

const hTC_Portal = new Entity()
hTC_Portal.setParent(scene)
const gltfShape_2 = new GLTFShape('models/HTC_Portal/HTC_Portal.glb')
hTC_Portal.addComponentOrReplace(gltfShape_2)
const transform_6 = new Transform({
  position: new Vector3(16, 0.5, 15.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
hTC_Portal.addComponentOrReplace(transform_6)
engine.addEntity(hTC_Portal)

const pond_01 = new Entity()
pond_01.setParent(scene)
const gltfShape_3 = new GLTFShape('models/Pond_01/Pond_01.glb')
pond_01.addComponentOrReplace(gltfShape_3)
const transform_7 = new Transform({
  position: new Vector3(16, 0, 16),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
pond_01.addComponentOrReplace(transform_7)
engine.addEntity(pond_01)

const spiralStone_02 = new Entity()
spiralStone_02.setParent(scene)
const gltfShape_4 = new GLTFShape('models/SpiralStone_02/SpiralStone_02.glb')
spiralStone_02.addComponentOrReplace(gltfShape_4)
const transform_8 = new Transform({
  position: new Vector3(17.5, 0, 11),
  rotation: new Quaternion(0, -0.923879532511287, 0, 0.3826834323650899),
  scale: new Vector3(1, 1, 1)
})
spiralStone_02.addComponentOrReplace(transform_8)
engine.addEntity(spiralStone_02)

const spiralStone_01 = new Entity()
spiralStone_01.setParent(scene)
const gltfShape_5 = new GLTFShape('models/SpiralStone_01/SpiralStone_01.glb')
spiralStone_01.addComponentOrReplace(gltfShape_5)
const transform_9 = new Transform({
  position: new Vector3(16, 0, 11),
  rotation: new Quaternion(0, -0.4713967368259978, 0, 0.8819212643483554),
  scale: new Vector3(1, 1, 1)
})
spiralStone_01.addComponentOrReplace(transform_9)
engine.addEntity(spiralStone_01)

const spiralStone_03 = new Entity()
spiralStone_03.setParent(scene)
const gltfShape_6 = new GLTFShape('models/SpiralStone_03/SpiralStone_03.glb')
spiralStone_03.addComponentOrReplace(gltfShape_6)
const transform_10 = new Transform({
  position: new Vector3(14.5, 0, 11),
  rotation: new Quaternion(0, 1, 0, 1.1102230246251565e-16),
  scale: new Vector3(1, 1, 1)
})
spiralStone_03.addComponentOrReplace(transform_10)
engine.addEntity(spiralStone_03)

const fenceStoneLarge_01 = new Entity()
fenceStoneLarge_01.setParent(scene)
const gltfShape_7 = new GLTFShape('models/FenceStoneLarge_01/FenceStoneLarge_01.glb')
fenceStoneLarge_01.addComponentOrReplace(gltfShape_7)
const transform_11 = new Transform({
  position: new Vector3(24, 0, 23),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceStoneLarge_01.addComponentOrReplace(transform_11)
engine.addEntity(fenceStoneLarge_01)

const fenceStoneLarge_01_2 = new Entity()
fenceStoneLarge_01_2.setParent(scene)
fenceStoneLarge_01_2.addComponentOrReplace(gltfShape_7)
const transform_12 = new Transform({
  position: new Vector3(24, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceStoneLarge_01_2.addComponentOrReplace(transform_12)
engine.addEntity(fenceStoneLarge_01_2)

const fenceStoneLarge_01_3 = new Entity()
fenceStoneLarge_01_3.setParent(scene)
fenceStoneLarge_01_3.addComponentOrReplace(gltfShape_7)
const transform_13 = new Transform({
  position: new Vector3(24, 0, 18),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceStoneLarge_01_3.addComponentOrReplace(transform_13)
engine.addEntity(fenceStoneLarge_01_3)

const fenceStoneSmall_01 = new Entity()
fenceStoneSmall_01.setParent(scene)
const gltfShape_8 = new GLTFShape('models/FenceStoneSmall_01/FenceStoneSmall_01.glb')
fenceStoneSmall_01.addComponentOrReplace(gltfShape_8)
const transform_14 = new Transform({
  position: new Vector3(24, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceStoneSmall_01.addComponentOrReplace(transform_14)
engine.addEntity(fenceStoneSmall_01)

const fenceStoneLarge_01_4 = new Entity()
fenceStoneLarge_01_4.setParent(scene)
fenceStoneLarge_01_4.addComponentOrReplace(gltfShape_7)
const transform_15 = new Transform({
  position: new Vector3(8, 0, 23),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceStoneLarge_01_4.addComponentOrReplace(transform_15)
engine.addEntity(fenceStoneLarge_01_4)

const fenceStoneLarge_01_5 = new Entity()
fenceStoneLarge_01_5.setParent(scene)
fenceStoneLarge_01_5.addComponentOrReplace(gltfShape_7)
const transform_16 = new Transform({
  position: new Vector3(8, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceStoneLarge_01_5.addComponentOrReplace(transform_16)
engine.addEntity(fenceStoneLarge_01_5)

const fenceStoneLarge_01_6 = new Entity()
fenceStoneLarge_01_6.setParent(scene)
fenceStoneLarge_01_6.addComponentOrReplace(gltfShape_7)
const transform_17 = new Transform({
  position: new Vector3(8, 0, 18),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceStoneLarge_01_6.addComponentOrReplace(transform_17)
engine.addEntity(fenceStoneLarge_01_6)

const fenceStoneLarge_01_7 = new Entity()
fenceStoneLarge_01_7.setParent(scene)
fenceStoneLarge_01_7.addComponentOrReplace(gltfShape_7)
const transform_18 = new Transform({
  position: new Vector3(8, 0, 18),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceStoneLarge_01_7.addComponentOrReplace(transform_18)
engine.addEntity(fenceStoneLarge_01_7)

const fenceStoneSmall_01_2 = new Entity()
fenceStoneSmall_01_2.setParent(scene)
fenceStoneSmall_01_2.addComponentOrReplace(gltfShape_8)
const transform_19 = new Transform({
  position: new Vector3(8, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceStoneSmall_01_2.addComponentOrReplace(transform_19)
engine.addEntity(fenceStoneSmall_01_2)

const fenceStoneSmall_01_3 = new Entity()
fenceStoneSmall_01_3.setParent(scene)
fenceStoneSmall_01_3.addComponentOrReplace(gltfShape_8)
const transform_20 = new Transform({
  position: new Vector3(9, 0, 24),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fenceStoneSmall_01_3.addComponentOrReplace(transform_20)
engine.addEntity(fenceStoneSmall_01_3)

const treeTall_01 = new Entity()
treeTall_01.setParent(scene)
const gltfShape_9 = new GLTFShape('models/TreeTall_01/TreeTall_01.glb')
treeTall_01.addComponentOrReplace(gltfShape_9)
const transform_21 = new Transform({
  position: new Vector3(21.5, 0, 21.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeTall_01.addComponentOrReplace(transform_21)
engine.addEntity(treeTall_01)

const treeTall_02 = new Entity()
treeTall_02.setParent(scene)
const gltfShape_10 = new GLTFShape('models/TreeTall_02/TreeTall_02.glb')
treeTall_02.addComponentOrReplace(gltfShape_10)
const transform_22 = new Transform({
  position: new Vector3(20.5, 0, 14),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeTall_02.addComponentOrReplace(transform_22)
engine.addEntity(treeTall_02)

const treeTall_03 = new Entity()
treeTall_03.setParent(scene)
const gltfShape_11 = new GLTFShape('models/TreeTall_03/TreeTall_03.glb')
treeTall_03.addComponentOrReplace(gltfShape_11)
const transform_23 = new Transform({
  position: new Vector3(23, 0, 11.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeTall_03.addComponentOrReplace(transform_23)
engine.addEntity(treeTall_03)

const treeSycamore_01 = new Entity()
treeSycamore_01.setParent(scene)
const gltfShape_12 = new GLTFShape('models/TreeSycamore_01/TreeSycamore_01.glb')
treeSycamore_01.addComponentOrReplace(gltfShape_12)
const transform_24 = new Transform({
  position: new Vector3(8.5, 0, 20),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeSycamore_01.addComponentOrReplace(transform_24)
engine.addEntity(treeSycamore_01)

const treeSycamore_03 = new Entity()
treeSycamore_03.setParent(scene)
const gltfShape_13 = new GLTFShape('models/TreeSycamore_03/TreeSycamore_03.glb')
treeSycamore_03.addComponentOrReplace(gltfShape_13)
const transform_25 = new Transform({
  position: new Vector3(11.5, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeSycamore_03.addComponentOrReplace(transform_25)
engine.addEntity(treeSycamore_03)

const treeSycamore_01_2 = new Entity()
treeSycamore_01_2.setParent(scene)
treeSycamore_01_2.addComponentOrReplace(gltfShape_12)
const transform_26 = new Transform({
  position: new Vector3(18, 0, 19),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeSycamore_01_2.addComponentOrReplace(transform_26)
engine.addEntity(treeSycamore_01_2)

const treeSycamore_02 = new Entity()
treeSycamore_02.setParent(scene)
const gltfShape_14 = new GLTFShape('models/TreeSycamore_02/TreeSycamore_02.glb')
treeSycamore_02.addComponentOrReplace(gltfShape_14)
const transform_27 = new Transform({
  position: new Vector3(20.5, 0, 9.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeSycamore_02.addComponentOrReplace(transform_27)
engine.addEntity(treeSycamore_02)

const treeTall_02_2 = new Entity()
treeTall_02_2.setParent(scene)
treeTall_02_2.addComponentOrReplace(gltfShape_10)
const transform_28 = new Transform({
  position: new Vector3(12, 0, 20.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeTall_02_2.addComponentOrReplace(transform_28)
engine.addEntity(treeTall_02_2)

const treeSycamore_02_2 = new Entity()
treeSycamore_02_2.setParent(scene)
treeSycamore_02_2.addComponentOrReplace(gltfShape_14)
const transform_29 = new Transform({
  position: new Vector3(12, 0, 13.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeSycamore_02_2.addComponentOrReplace(transform_29)
engine.addEntity(treeSycamore_02_2)

const floorBlock_05 = new Entity()
floorBlock_05.setParent(scene)
const gltfShape_15 = new GLTFShape('models/FloorBlock_05/FloorBlock_05.glb')
floorBlock_05.addComponentOrReplace(gltfShape_15)
const transform_30 = new Transform({
  position: new Vector3(17.5, 0, 23),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05.addComponentOrReplace(transform_30)
engine.addEntity(floorBlock_05)

const floorBlock_05_2 = new Entity()
floorBlock_05_2.setParent(scene)
floorBlock_05_2.addComponentOrReplace(gltfShape_15)
const transform_31 = new Transform({
  position: new Vector3(19, 0, 17),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_2.addComponentOrReplace(transform_31)
engine.addEntity(floorBlock_05_2)

const floorBlock_05_3 = new Entity()
floorBlock_05_3.setParent(scene)
floorBlock_05_3.addComponentOrReplace(gltfShape_15)
const transform_32 = new Transform({
  position: new Vector3(21, 0, 23),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_3.addComponentOrReplace(transform_32)
engine.addEntity(floorBlock_05_3)

const floorBlock_05_4 = new Entity()
floorBlock_05_4.setParent(scene)
floorBlock_05_4.addComponentOrReplace(gltfShape_15)
const transform_33 = new Transform({
  position: new Vector3(19.5, 0, 23),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_4.addComponentOrReplace(transform_33)
engine.addEntity(floorBlock_05_4)

const floorBlock_05_5 = new Entity()
floorBlock_05_5.setParent(scene)
floorBlock_05_5.addComponentOrReplace(gltfShape_15)
const transform_34 = new Transform({
  position: new Vector3(23, 0, 23),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_5.addComponentOrReplace(transform_34)
engine.addEntity(floorBlock_05_5)

const floorBlock_05_6 = new Entity()
floorBlock_05_6.setParent(scene)
floorBlock_05_6.addComponentOrReplace(gltfShape_15)
const transform_35 = new Transform({
  position: new Vector3(15.5, 0, 23),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_6.addComponentOrReplace(transform_35)
engine.addEntity(floorBlock_05_6)

const floorBlock_05_7 = new Entity()
floorBlock_05_7.setParent(scene)
floorBlock_05_7.addComponentOrReplace(gltfShape_15)
const transform_36 = new Transform({
  position: new Vector3(13.5, 0, 23),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_7.addComponentOrReplace(transform_36)
engine.addEntity(floorBlock_05_7)

const floorBlock_05_8 = new Entity()
floorBlock_05_8.setParent(scene)
floorBlock_05_8.addComponentOrReplace(gltfShape_15)
const transform_37 = new Transform({
  position: new Vector3(11.5, 0, 23),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_8.addComponentOrReplace(transform_37)
engine.addEntity(floorBlock_05_8)

const floorBlock_05_9 = new Entity()
floorBlock_05_9.setParent(scene)
floorBlock_05_9.addComponentOrReplace(gltfShape_15)
const transform_38 = new Transform({
  position: new Vector3(9.5, 0, 23),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_9.addComponentOrReplace(transform_38)
engine.addEntity(floorBlock_05_9)

const floorBlock_05_10 = new Entity()
floorBlock_05_10.setParent(scene)
floorBlock_05_10.addComponentOrReplace(gltfShape_15)
const transform_39 = new Transform({
  position: new Vector3(9, 0, 23),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_10.addComponentOrReplace(transform_39)
engine.addEntity(floorBlock_05_10)

const floorBlock_05_11 = new Entity()
floorBlock_05_11.setParent(scene)
floorBlock_05_11.addComponentOrReplace(gltfShape_15)
const transform_40 = new Transform({
  position: new Vector3(9, 0, 21),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_11.addComponentOrReplace(transform_40)
engine.addEntity(floorBlock_05_11)

const floorBlock_05_12 = new Entity()
floorBlock_05_12.setParent(scene)
floorBlock_05_12.addComponentOrReplace(gltfShape_15)
const transform_41 = new Transform({
  position: new Vector3(9, 0, 19),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_12.addComponentOrReplace(transform_41)
engine.addEntity(floorBlock_05_12)

const floorBlock_05_13 = new Entity()
floorBlock_05_13.setParent(scene)
floorBlock_05_13.addComponentOrReplace(gltfShape_15)
const transform_42 = new Transform({
  position: new Vector3(9, 0, 17),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_13.addComponentOrReplace(transform_42)
engine.addEntity(floorBlock_05_13)

const floorBlock_05_14 = new Entity()
floorBlock_05_14.setParent(scene)
floorBlock_05_14.addComponentOrReplace(gltfShape_15)
const transform_43 = new Transform({
  position: new Vector3(9, 0, 15),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_14.addComponentOrReplace(transform_43)
engine.addEntity(floorBlock_05_14)

const floorBlock_05_15 = new Entity()
floorBlock_05_15.setParent(scene)
floorBlock_05_15.addComponentOrReplace(gltfShape_15)
const transform_44 = new Transform({
  position: new Vector3(9, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_15.addComponentOrReplace(transform_44)
engine.addEntity(floorBlock_05_15)

const floorBlock_05_16 = new Entity()
floorBlock_05_16.setParent(scene)
floorBlock_05_16.addComponentOrReplace(gltfShape_15)
const transform_45 = new Transform({
  position: new Vector3(9, 0, 11),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_16.addComponentOrReplace(transform_45)
engine.addEntity(floorBlock_05_16)

const floorBlock_05_17 = new Entity()
floorBlock_05_17.setParent(scene)
floorBlock_05_17.addComponentOrReplace(gltfShape_15)
const transform_46 = new Transform({
  position: new Vector3(9, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_17.addComponentOrReplace(transform_46)
engine.addEntity(floorBlock_05_17)

const floorBlock_05_18 = new Entity()
floorBlock_05_18.setParent(scene)
floorBlock_05_18.addComponentOrReplace(gltfShape_15)
const transform_47 = new Transform({
  position: new Vector3(11, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_18.addComponentOrReplace(transform_47)
engine.addEntity(floorBlock_05_18)

const floorBlock_05_19 = new Entity()
floorBlock_05_19.setParent(scene)
floorBlock_05_19.addComponentOrReplace(gltfShape_15)
const transform_48 = new Transform({
  position: new Vector3(13, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_19.addComponentOrReplace(transform_48)
engine.addEntity(floorBlock_05_19)

const floorBlock_05_20 = new Entity()
floorBlock_05_20.setParent(scene)
floorBlock_05_20.addComponentOrReplace(gltfShape_15)
const transform_49 = new Transform({
  position: new Vector3(15, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_20.addComponentOrReplace(transform_49)
engine.addEntity(floorBlock_05_20)

const floorBlock_05_21 = new Entity()
floorBlock_05_21.setParent(scene)
floorBlock_05_21.addComponentOrReplace(gltfShape_15)
const transform_50 = new Transform({
  position: new Vector3(17, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_21.addComponentOrReplace(transform_50)
engine.addEntity(floorBlock_05_21)

const floorBlock_05_22 = new Entity()
floorBlock_05_22.setParent(scene)
floorBlock_05_22.addComponentOrReplace(gltfShape_15)
const transform_51 = new Transform({
  position: new Vector3(19, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_22.addComponentOrReplace(transform_51)
engine.addEntity(floorBlock_05_22)

const floorBlock_05_23 = new Entity()
floorBlock_05_23.setParent(scene)
floorBlock_05_23.addComponentOrReplace(gltfShape_15)
const transform_52 = new Transform({
  position: new Vector3(21, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_23.addComponentOrReplace(transform_52)
engine.addEntity(floorBlock_05_23)

const floorBlock_05_24 = new Entity()
floorBlock_05_24.setParent(scene)
floorBlock_05_24.addComponentOrReplace(gltfShape_15)
const transform_53 = new Transform({
  position: new Vector3(23, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_24.addComponentOrReplace(transform_53)
engine.addEntity(floorBlock_05_24)

const floorBlock_05_25 = new Entity()
floorBlock_05_25.setParent(scene)
floorBlock_05_25.addComponentOrReplace(gltfShape_15)
const transform_54 = new Transform({
  position: new Vector3(23, 0, 11),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_25.addComponentOrReplace(transform_54)
engine.addEntity(floorBlock_05_25)

const floorBlock_05_26 = new Entity()
floorBlock_05_26.setParent(scene)
floorBlock_05_26.addComponentOrReplace(gltfShape_15)
const transform_55 = new Transform({
  position: new Vector3(23, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_26.addComponentOrReplace(transform_55)
engine.addEntity(floorBlock_05_26)

const floorBlock_05_27 = new Entity()
floorBlock_05_27.setParent(scene)
floorBlock_05_27.addComponentOrReplace(gltfShape_15)
const transform_56 = new Transform({
  position: new Vector3(23, 0, 15),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_27.addComponentOrReplace(transform_56)
engine.addEntity(floorBlock_05_27)

const floorBlock_05_28 = new Entity()
floorBlock_05_28.setParent(scene)
floorBlock_05_28.addComponentOrReplace(gltfShape_15)
const transform_57 = new Transform({
  position: new Vector3(23, 0, 17),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_28.addComponentOrReplace(transform_57)
engine.addEntity(floorBlock_05_28)

const floorBlock_05_29 = new Entity()
floorBlock_05_29.setParent(scene)
floorBlock_05_29.addComponentOrReplace(gltfShape_15)
const transform_58 = new Transform({
  position: new Vector3(23, 0, 19),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_29.addComponentOrReplace(transform_58)
engine.addEntity(floorBlock_05_29)

const floorBlock_05_30 = new Entity()
floorBlock_05_30.setParent(scene)
floorBlock_05_30.addComponentOrReplace(gltfShape_15)
const transform_59 = new Transform({
  position: new Vector3(23, 0, 21),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_30.addComponentOrReplace(transform_59)
engine.addEntity(floorBlock_05_30)

const floorBlock_05_31 = new Entity()
floorBlock_05_31.setParent(scene)
floorBlock_05_31.addComponentOrReplace(gltfShape_15)
const transform_60 = new Transform({
  position: new Vector3(21, 0, 21),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_31.addComponentOrReplace(transform_60)
engine.addEntity(floorBlock_05_31)

const floorBlock_05_32 = new Entity()
floorBlock_05_32.setParent(scene)
floorBlock_05_32.addComponentOrReplace(gltfShape_15)
const transform_61 = new Transform({
  position: new Vector3(19, 0, 21),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_32.addComponentOrReplace(transform_61)
engine.addEntity(floorBlock_05_32)

const floorBlock_05_33 = new Entity()
floorBlock_05_33.setParent(scene)
floorBlock_05_33.addComponentOrReplace(gltfShape_15)
const transform_62 = new Transform({
  position: new Vector3(17, 0, 21),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_33.addComponentOrReplace(transform_62)
engine.addEntity(floorBlock_05_33)

const floorBlock_05_34 = new Entity()
floorBlock_05_34.setParent(scene)
floorBlock_05_34.addComponentOrReplace(gltfShape_15)
const transform_63 = new Transform({
  position: new Vector3(15, 0, 21),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_34.addComponentOrReplace(transform_63)
engine.addEntity(floorBlock_05_34)

const floorBlock_05_35 = new Entity()
floorBlock_05_35.setParent(scene)
floorBlock_05_35.addComponentOrReplace(gltfShape_15)
const transform_64 = new Transform({
  position: new Vector3(13, 0, 21),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_35.addComponentOrReplace(transform_64)
engine.addEntity(floorBlock_05_35)

const floorBlock_05_36 = new Entity()
floorBlock_05_36.setParent(scene)
floorBlock_05_36.addComponentOrReplace(gltfShape_15)
const transform_65 = new Transform({
  position: new Vector3(11, 0, 21),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_36.addComponentOrReplace(transform_65)
engine.addEntity(floorBlock_05_36)

const floorBlock_05_37 = new Entity()
floorBlock_05_37.setParent(scene)
floorBlock_05_37.addComponentOrReplace(gltfShape_15)
const transform_66 = new Transform({
  position: new Vector3(11, 0, 19),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_37.addComponentOrReplace(transform_66)
engine.addEntity(floorBlock_05_37)

const floorBlock_05_38 = new Entity()
floorBlock_05_38.setParent(scene)
floorBlock_05_38.addComponentOrReplace(gltfShape_15)
const transform_67 = new Transform({
  position: new Vector3(11, 0, 17),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_38.addComponentOrReplace(transform_67)
engine.addEntity(floorBlock_05_38)

const floorBlock_05_39 = new Entity()
floorBlock_05_39.setParent(scene)
floorBlock_05_39.addComponentOrReplace(gltfShape_15)
const transform_68 = new Transform({
  position: new Vector3(11, 0, 15),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_39.addComponentOrReplace(transform_68)
engine.addEntity(floorBlock_05_39)

const floorBlock_05_40 = new Entity()
floorBlock_05_40.setParent(scene)
floorBlock_05_40.addComponentOrReplace(gltfShape_15)
const transform_69 = new Transform({
  position: new Vector3(11, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_40.addComponentOrReplace(transform_69)
engine.addEntity(floorBlock_05_40)

const floorBlock_05_41 = new Entity()
floorBlock_05_41.setParent(scene)
floorBlock_05_41.addComponentOrReplace(gltfShape_15)
const transform_70 = new Transform({
  position: new Vector3(11, 0, 11),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_41.addComponentOrReplace(transform_70)
engine.addEntity(floorBlock_05_41)

const floorBlock_05_42 = new Entity()
floorBlock_05_42.setParent(scene)
floorBlock_05_42.addComponentOrReplace(gltfShape_15)
const transform_71 = new Transform({
  position: new Vector3(13, 0, 11),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_42.addComponentOrReplace(transform_71)
engine.addEntity(floorBlock_05_42)

const floorBlock_05_43 = new Entity()
floorBlock_05_43.setParent(scene)
floorBlock_05_43.addComponentOrReplace(gltfShape_15)
const transform_72 = new Transform({
  position: new Vector3(15, 0, 11),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_43.addComponentOrReplace(transform_72)
engine.addEntity(floorBlock_05_43)

const floorBlock_05_44 = new Entity()
floorBlock_05_44.setParent(scene)
floorBlock_05_44.addComponentOrReplace(gltfShape_15)
const transform_73 = new Transform({
  position: new Vector3(17, 0, 11),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_44.addComponentOrReplace(transform_73)
engine.addEntity(floorBlock_05_44)

const floorBlock_05_45 = new Entity()
floorBlock_05_45.setParent(scene)
floorBlock_05_45.addComponentOrReplace(gltfShape_15)
const transform_74 = new Transform({
  position: new Vector3(19, 0, 11),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_45.addComponentOrReplace(transform_74)
engine.addEntity(floorBlock_05_45)

const floorBlock_05_46 = new Entity()
floorBlock_05_46.setParent(scene)
floorBlock_05_46.addComponentOrReplace(gltfShape_15)
const transform_75 = new Transform({
  position: new Vector3(21, 0, 11),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_46.addComponentOrReplace(transform_75)
engine.addEntity(floorBlock_05_46)

const floorBlock_05_47 = new Entity()
floorBlock_05_47.setParent(scene)
floorBlock_05_47.addComponentOrReplace(gltfShape_15)
const transform_76 = new Transform({
  position: new Vector3(21, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_47.addComponentOrReplace(transform_76)
engine.addEntity(floorBlock_05_47)

const floorBlock_05_48 = new Entity()
floorBlock_05_48.setParent(scene)
floorBlock_05_48.addComponentOrReplace(gltfShape_15)
const transform_77 = new Transform({
  position: new Vector3(21, 0, 15),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_48.addComponentOrReplace(transform_77)
engine.addEntity(floorBlock_05_48)

const floorBlock_05_49 = new Entity()
floorBlock_05_49.setParent(scene)
floorBlock_05_49.addComponentOrReplace(gltfShape_15)
const transform_78 = new Transform({
  position: new Vector3(21, 0, 17),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_49.addComponentOrReplace(transform_78)
engine.addEntity(floorBlock_05_49)

const floorBlock_05_50 = new Entity()
floorBlock_05_50.setParent(scene)
floorBlock_05_50.addComponentOrReplace(gltfShape_15)
const transform_79 = new Transform({
  position: new Vector3(21, 0, 19),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_50.addComponentOrReplace(transform_79)
engine.addEntity(floorBlock_05_50)

const floorBlock_05_51 = new Entity()
floorBlock_05_51.setParent(scene)
floorBlock_05_51.addComponentOrReplace(gltfShape_15)
const transform_80 = new Transform({
  position: new Vector3(19, 0, 19),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_51.addComponentOrReplace(transform_80)
engine.addEntity(floorBlock_05_51)

const floorBlock_05_52 = new Entity()
floorBlock_05_52.setParent(scene)
floorBlock_05_52.addComponentOrReplace(gltfShape_15)
const transform_81 = new Transform({
  position: new Vector3(17, 0, 19),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_52.addComponentOrReplace(transform_81)
engine.addEntity(floorBlock_05_52)

const floorBlock_05_53 = new Entity()
floorBlock_05_53.setParent(scene)
floorBlock_05_53.addComponentOrReplace(gltfShape_15)
const transform_82 = new Transform({
  position: new Vector3(15.5, 0, 19),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_53.addComponentOrReplace(transform_82)
engine.addEntity(floorBlock_05_53)

const floorBlock_05_54 = new Entity()
floorBlock_05_54.setParent(scene)
floorBlock_05_54.addComponentOrReplace(gltfShape_15)
const transform_83 = new Transform({
  position: new Vector3(19, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_54.addComponentOrReplace(transform_83)
engine.addEntity(floorBlock_05_54)

const floorBlock_05_55 = new Entity()
floorBlock_05_55.setParent(scene)
floorBlock_05_55.addComponentOrReplace(gltfShape_15)
const transform_84 = new Transform({
  position: new Vector3(13.5, 0, 17),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_55.addComponentOrReplace(transform_84)
engine.addEntity(floorBlock_05_55)

const floorBlock_05_56 = new Entity()
floorBlock_05_56.setParent(scene)
floorBlock_05_56.addComponentOrReplace(gltfShape_15)
const transform_85 = new Transform({
  position: new Vector3(13.5, 0, 15),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_56.addComponentOrReplace(transform_85)
engine.addEntity(floorBlock_05_56)

const floorBlock_05_57 = new Entity()
floorBlock_05_57.setParent(scene)
floorBlock_05_57.addComponentOrReplace(gltfShape_15)
const transform_86 = new Transform({
  position: new Vector3(13.5, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_57.addComponentOrReplace(transform_86)
engine.addEntity(floorBlock_05_57)

const floorBlock_05_58 = new Entity()
floorBlock_05_58.setParent(scene)
floorBlock_05_58.addComponentOrReplace(gltfShape_15)
const transform_87 = new Transform({
  position: new Vector3(15.5, 0, 13.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_58.addComponentOrReplace(transform_87)
engine.addEntity(floorBlock_05_58)

const floorBlock_05_59 = new Entity()
floorBlock_05_59.setParent(scene)
floorBlock_05_59.addComponentOrReplace(gltfShape_15)
const transform_88 = new Transform({
  position: new Vector3(17.5, 0, 13.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_59.addComponentOrReplace(transform_88)
engine.addEntity(floorBlock_05_59)

const floorBlock_05_60 = new Entity()
floorBlock_05_60.setParent(scene)
floorBlock_05_60.addComponentOrReplace(gltfShape_15)
const transform_89 = new Transform({
  position: new Vector3(19, 0, 13.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_60.addComponentOrReplace(transform_89)
engine.addEntity(floorBlock_05_60)

const floorBlock_05_61 = new Entity()
floorBlock_05_61.setParent(scene)
floorBlock_05_61.addComponentOrReplace(gltfShape_15)
const transform_90 = new Transform({
  position: new Vector3(18.5, 0, 15),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_61.addComponentOrReplace(transform_90)
engine.addEntity(floorBlock_05_61)

const floorBlock_05_62 = new Entity()
floorBlock_05_62.setParent(scene)
floorBlock_05_62.addComponentOrReplace(gltfShape_15)
const transform_91 = new Transform({
  position: new Vector3(19.5, 0, 15),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_62.addComponentOrReplace(transform_91)
engine.addEntity(floorBlock_05_62)

const floorBlock_05_63 = new Entity()
floorBlock_05_63.setParent(scene)
floorBlock_05_63.addComponentOrReplace(gltfShape_15)
const transform_92 = new Transform({
  position: new Vector3(18, 0, 17),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_63.addComponentOrReplace(transform_92)
engine.addEntity(floorBlock_05_63)

const floorBlock_05_64 = new Entity()
floorBlock_05_64.setParent(scene)
floorBlock_05_64.addComponentOrReplace(gltfShape_15)
const transform_93 = new Transform({
  position: new Vector3(13.5, 0, 19),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_64.addComponentOrReplace(transform_93)
engine.addEntity(floorBlock_05_64)

const floorBlock_05_65 = new Entity()
floorBlock_05_65.setParent(scene)
floorBlock_05_65.addComponentOrReplace(gltfShape_15)
const transform_94 = new Transform({
  position: new Vector3(13, 0, 19),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_65.addComponentOrReplace(transform_94)
engine.addEntity(floorBlock_05_65)

const floorBlock_05_66 = new Entity()
floorBlock_05_66.setParent(scene)
floorBlock_05_66.addComponentOrReplace(gltfShape_15)
const transform_95 = new Transform({
  position: new Vector3(13, 0, 17),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_66.addComponentOrReplace(transform_95)
engine.addEntity(floorBlock_05_66)

const floorBlock_05_67 = new Entity()
floorBlock_05_67.setParent(scene)
floorBlock_05_67.addComponentOrReplace(gltfShape_15)
const transform_96 = new Transform({
  position: new Vector3(13, 0, 15.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_67.addComponentOrReplace(transform_96)
engine.addEntity(floorBlock_05_67)

const floorBlock_05_68 = new Entity()
floorBlock_05_68.setParent(scene)
floorBlock_05_68.addComponentOrReplace(gltfShape_15)
const transform_97 = new Transform({
  position: new Vector3(13, 0, 13.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_68.addComponentOrReplace(transform_97)
engine.addEntity(floorBlock_05_68)

const floorBlock_05_69 = new Entity()
floorBlock_05_69.setParent(scene)
floorBlock_05_69.addComponentOrReplace(gltfShape_15)
const transform_98 = new Transform({
  position: new Vector3(13, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_69.addComponentOrReplace(transform_98)
engine.addEntity(floorBlock_05_69)

const floorBlock_05_70 = new Entity()
floorBlock_05_70.setParent(scene)
floorBlock_05_70.addComponentOrReplace(gltfShape_15)
const transform_99 = new Transform({
  position: new Vector3(15.5, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_70.addComponentOrReplace(transform_99)
engine.addEntity(floorBlock_05_70)

const floorBlock_05_71 = new Entity()
floorBlock_05_71.setParent(scene)
floorBlock_05_71.addComponentOrReplace(gltfShape_15)
const transform_100 = new Transform({
  position: new Vector3(17.5, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_05_71.addComponentOrReplace(transform_100)
engine.addEntity(floorBlock_05_71)

const cactus_01 = new Entity()
cactus_01.setParent(scene)
const gltfShape_16 = new GLTFShape('models/Cactus_01/Cactus_01.glb')
cactus_01.addComponentOrReplace(gltfShape_16)
const transform_101 = new Transform({
  position: new Vector3(22, 0, 1.5),
  rotation: new Quaternion(0, -0.47139673682599786, 0, 0.8819212643483552),
  scale: new Vector3(1, 1, 1)
})
cactus_01.addComponentOrReplace(transform_101)
engine.addEntity(cactus_01)

const cactus_01_2 = new Entity()
cactus_01_2.setParent(scene)
cactus_01_2.addComponentOrReplace(gltfShape_16)
const transform_102 = new Transform({
  position: new Vector3(11, 0, 3),
  rotation: new Quaternion(0, -0.47139673682599786, 0, 0.8819212643483552),
  scale: new Vector3(1, 1, 1)
})
cactus_01_2.addComponentOrReplace(transform_102)
engine.addEntity(cactus_01_2)

const chineseStatueDragon_01 = new Entity()
chineseStatueDragon_01.setParent(scene)
const gltfShape_17 = new GLTFShape('models/ChineseStatueDragon_01/ChineseStatueDragon_01.glb')
chineseStatueDragon_01.addComponentOrReplace(gltfShape_17)
const transform_103 = new Transform({
  position: new Vector3(21.5, 0, 17.5),
  rotation: new Quaternion(0, 0.9569403357322087, 0, -0.2902846772544622),
  scale: new Vector3(1, 1, 1)
})
chineseStatueDragon_01.addComponentOrReplace(transform_103)
engine.addEntity(chineseStatueDragon_01)

const chineseGate_03 = new Entity()
chineseGate_03.setParent(scene)
const gltfShape_18 = new GLTFShape('models/ChineseGate_03/ChineseGate_03.glb')
chineseGate_03.addComponentOrReplace(gltfShape_18)
const transform_104 = new Transform({
  position: new Vector3(16, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
chineseGate_03.addComponentOrReplace(transform_104)
engine.addEntity(chineseGate_03)

const chineseFountain_03 = new Entity()
chineseFountain_03.setParent(scene)
const gltfShape_19 = new GLTFShape('models/ChineseLampPost_01/ChineseFountain_03.glb')
chineseFountain_03.addComponentOrReplace(gltfShape_19)
const transform_105 = new Transform({
  position: new Vector3(11, 0, 18),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
chineseFountain_03.addComponentOrReplace(transform_105)
engine.addEntity(chineseFountain_03)

const chineseHouse_01 = new Entity()
chineseHouse_01.setParent(scene)
const gltfShape_20 = new GLTFShape('models/ChineseHouse_01/ChineseHouse_01.glb')
chineseHouse_01.addComponentOrReplace(gltfShape_20)
const transform_106 = new Transform({
  position: new Vector3(16, 0, 27),
  rotation: new Quaternion(0, -1, 0, 9.71445146547012e-17),
  scale: new Vector3(1, 1, 1)
})
chineseHouse_01.addComponentOrReplace(transform_106)
engine.addEntity(chineseHouse_01)

const fenceStoneSmall_01_4 = new Entity()
fenceStoneSmall_01_4.setParent(scene)
fenceStoneSmall_01_4.addComponentOrReplace(gltfShape_8)
const transform_107 = new Transform({
  position: new Vector3(23, 0, 24),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fenceStoneSmall_01_4.addComponentOrReplace(transform_107)
engine.addEntity(fenceStoneSmall_01_4)

const fenceStoneSmall_01_5 = new Entity()
fenceStoneSmall_01_5.setParent(scene)
fenceStoneSmall_01_5.addComponentOrReplace(gltfShape_8)
const transform_108 = new Transform({
  position: new Vector3(22.5, 0, 24),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fenceStoneSmall_01_5.addComponentOrReplace(transform_108)
engine.addEntity(fenceStoneSmall_01_5)

const fenceStoneSmall_01_6 = new Entity()
fenceStoneSmall_01_6.setParent(scene)
fenceStoneSmall_01_6.addComponentOrReplace(gltfShape_8)
const transform_109 = new Transform({
  position: new Vector3(22, 0, 24),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fenceStoneSmall_01_6.addComponentOrReplace(transform_109)
engine.addEntity(fenceStoneSmall_01_6)

const fenceStoneSmall_01_7 = new Entity()
fenceStoneSmall_01_7.setParent(scene)
fenceStoneSmall_01_7.addComponentOrReplace(gltfShape_8)
const transform_110 = new Transform({
  position: new Vector3(23, 0, 24),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fenceStoneSmall_01_7.addComponentOrReplace(transform_110)
engine.addEntity(fenceStoneSmall_01_7)

const fenceStoneSmall_01_8 = new Entity()
fenceStoneSmall_01_8.setParent(scene)
fenceStoneSmall_01_8.addComponentOrReplace(gltfShape_8)
const transform_111 = new Transform({
  position: new Vector3(8, 0, 24),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fenceStoneSmall_01_8.addComponentOrReplace(transform_111)
engine.addEntity(fenceStoneSmall_01_8)

const fenceStoneSmall_01_9 = new Entity()
fenceStoneSmall_01_9.setParent(scene)
fenceStoneSmall_01_9.addComponentOrReplace(gltfShape_8)
const transform_112 = new Transform({
  position: new Vector3(8.5, 0, 24),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fenceStoneSmall_01_9.addComponentOrReplace(transform_112)
engine.addEntity(fenceStoneSmall_01_9)

const fenceStoneLarge_01_8 = new Entity()
fenceStoneLarge_01_8.setParent(scene)
fenceStoneLarge_01_8.addComponentOrReplace(gltfShape_7)
const transform_113 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, -0.7071067811865477, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
fenceStoneLarge_01_8.addComponentOrReplace(transform_113)
engine.addEntity(fenceStoneLarge_01_8)

const fenceStoneLarge_01_9 = new Entity()
fenceStoneLarge_01_9.setParent(scene)
fenceStoneLarge_01_9.addComponentOrReplace(gltfShape_7)
const transform_114 = new Transform({
  position: new Vector3(19, 0, 8),
  rotation: new Quaternion(0, -0.7071067811865477, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
fenceStoneLarge_01_9.addComponentOrReplace(transform_114)
engine.addEntity(fenceStoneLarge_01_9)

const chineseFountain_01 = new Entity()
chineseFountain_01.setParent(scene)
const gltfShape_21 = new GLTFShape('models/ChineseFountain_01/ChineseFountain_01.glb')
chineseFountain_01.addComponentOrReplace(gltfShape_21)
const transform_115 = new Transform({
  position: new Vector3(9.5, 0, 14.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
chineseFountain_01.addComponentOrReplace(transform_115)
engine.addEntity(chineseFountain_01)

const chineseGate_01 = new Entity()
chineseGate_01.setParent(scene)
const gltfShape_22 = new GLTFShape('models/ChineseGong_01/ChineseGate_01.glb')
chineseGate_01.addComponentOrReplace(gltfShape_22)
const transform_116 = new Transform({
  position: new Vector3(19, 0, 4.5),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
chineseGate_01.addComponentOrReplace(transform_116)
engine.addEntity(chineseGate_01)

const decentralandLogo_01 = new Entity()
decentralandLogo_01.setParent(scene)
const gltfShape_23 = new GLTFShape('models/DecentralandLogo_01/DecentralandLogo_01.glb')
decentralandLogo_01.addComponentOrReplace(gltfShape_23)
const transform_117 = new Transform({
  position: new Vector3(1, 0, 30),
  rotation: new Quaternion(0.5000000000000001, -0.5000000000000002, -0.5, 0.5000000000000001),
  scale: new Vector3(1, 1, 1)
})
decentralandLogo_01.addComponentOrReplace(transform_117)
engine.addEntity(decentralandLogo_01)

const waterPatchCurve_02 = new Entity()
waterPatchCurve_02.setParent(scene)
const gltfShape_24 = new GLTFShape('models/WaterPatchCurve_02/WaterPatchCurve_02.glb')
waterPatchCurve_02.addComponentOrReplace(gltfShape_24)
const transform_118 = new Transform({
  position: new Vector3(16, 0, 16),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchCurve_02.addComponentOrReplace(transform_118)
engine.addEntity(waterPatchCurve_02)

const waterPatchCornerOutside_01 = new Entity()
waterPatchCornerOutside_01.setParent(scene)
const gltfShape_25 = new GLTFShape('models/WaterPatchCornerOutside_01/WaterPatchCornerOutside_01.glb')
waterPatchCornerOutside_01.addComponentOrReplace(gltfShape_25)
const transform_119 = new Transform({
  position: new Vector3(24, 0, 24),
  rotation: new Quaternion(0, -1.0000000000000002, 0, 1.6653345369377348e-16),
  scale: new Vector3(1, 1, 1)
})
waterPatchCornerOutside_01.addComponentOrReplace(transform_119)
engine.addEntity(waterPatchCornerOutside_01)

const floorBaseSand_01 = new Entity()
floorBaseSand_01.setParent(scene)
const gltfShape_26 = new GLTFShape('models/FloorBaseSand_01/FloorBaseSand_01.glb')
floorBaseSand_01.addComponentOrReplace(gltfShape_26)
const transform_120 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseSand_01.addComponentOrReplace(transform_120)
engine.addEntity(floorBaseSand_01)

const floorBaseSand_01_2 = new Entity()
floorBaseSand_01_2.setParent(scene)
floorBaseSand_01_2.addComponentOrReplace(gltfShape_26)
const transform_121 = new Transform({
  position: new Vector3(24, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseSand_01_2.addComponentOrReplace(transform_121)
engine.addEntity(floorBaseSand_01_2)

const floorBaseSand_01_3 = new Entity()
floorBaseSand_01_3.setParent(scene)
floorBaseSand_01_3.addComponentOrReplace(gltfShape_26)
const transform_122 = new Transform({
  position: new Vector3(8, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseSand_01_3.addComponentOrReplace(transform_122)
engine.addEntity(floorBaseSand_01_3)

const floorBaseSand_01_4 = new Entity()
floorBaseSand_01_4.setParent(scene)
floorBaseSand_01_4.addComponentOrReplace(gltfShape_26)
const transform_123 = new Transform({
  position: new Vector3(24, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseSand_01_4.addComponentOrReplace(transform_123)
engine.addEntity(floorBaseSand_01_4)

const waterPatchFull_01 = new Entity()
waterPatchFull_01.setParent(scene)
const gltfShape_27 = new GLTFShape('models/WaterPatchFull_01/WaterPatchFull_01.glb')
waterPatchFull_01.addComponentOrReplace(gltfShape_27)
const transform_124 = new Transform({
  position: new Vector3(16, 0, 16),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01.addComponentOrReplace(transform_124)
engine.addEntity(waterPatchFull_01)

const waterPatchCornerOutside_01_2 = new Entity()
waterPatchCornerOutside_01_2.setParent(scene)
waterPatchCornerOutside_01_2.addComponentOrReplace(gltfShape_25)
const transform_125 = new Transform({
  position: new Vector3(16, 0, 16),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchCornerOutside_01_2.addComponentOrReplace(transform_125)
engine.addEntity(waterPatchCornerOutside_01_2)

const waterPatchSide_01 = new Entity()
waterPatchSide_01.setParent(scene)
const gltfShape_28 = new GLTFShape('models/WaterPatchSide_01/WaterPatchSide_01.glb')
waterPatchSide_01.addComponentOrReplace(gltfShape_28)
const transform_126 = new Transform({
  position: new Vector3(16.5, 0, 24),
  rotation: new Quaternion(0, -1.0000000000000002, 0, 2.914335439641036e-16),
  scale: new Vector3(1, 1, 1)
})
waterPatchSide_01.addComponentOrReplace(transform_126)
engine.addEntity(waterPatchSide_01)

const waterPatchSide_01_2 = new Entity()
waterPatchSide_01_2.setParent(scene)
waterPatchSide_01_2.addComponentOrReplace(gltfShape_28)
const transform_127 = new Transform({
  position: new Vector3(24, 0, 16.5),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
waterPatchSide_01_2.addComponentOrReplace(transform_127)
engine.addEntity(waterPatchSide_01_2)

const waterPatchCurve_02_2 = new Entity()
waterPatchCurve_02_2.setParent(scene)
waterPatchCurve_02_2.addComponentOrReplace(gltfShape_24)
const transform_128 = new Transform({
  position: new Vector3(24, 0, 8.5),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
waterPatchCurve_02_2.addComponentOrReplace(transform_128)
engine.addEntity(waterPatchCurve_02_2)

const waterPatchCurve_01 = new Entity()
waterPatchCurve_01.setParent(scene)
const gltfShape_29 = new GLTFShape('models/WaterPatchCurve_01/WaterPatchCurve_01.glb')
waterPatchCurve_01.addComponentOrReplace(gltfShape_29)
const transform_129 = new Transform({
  position: new Vector3(24.5, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchCurve_01.addComponentOrReplace(transform_129)
engine.addEntity(waterPatchCurve_01)

const waterPatchSide_01_3 = new Entity()
waterPatchSide_01_3.setParent(scene)
waterPatchSide_01_3.addComponentOrReplace(gltfShape_28)
const transform_130 = new Transform({
  position: new Vector3(24, 0, 24.5),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
waterPatchSide_01_3.addComponentOrReplace(transform_130)
engine.addEntity(waterPatchSide_01_3)

const waterPatchSide_01_4 = new Entity()
waterPatchSide_01_4.setParent(scene)
waterPatchSide_01_4.addComponentOrReplace(gltfShape_28)
const transform_131 = new Transform({
  position: new Vector3(3.5, 0, 24),
  rotation: new Quaternion(0, 1, 0, 9.71445146547012e-17),
  scale: new Vector3(1, 1, 1)
})
waterPatchSide_01_4.addComponentOrReplace(transform_131)
engine.addEntity(waterPatchSide_01_4)

const waterPatchCornerOutside_01_3 = new Entity()
waterPatchCornerOutside_01_3.setParent(scene)
waterPatchCornerOutside_01_3.addComponentOrReplace(gltfShape_25)
const transform_132 = new Transform({
  position: new Vector3(8, 0, 24),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
waterPatchCornerOutside_01_3.addComponentOrReplace(transform_132)
engine.addEntity(waterPatchCornerOutside_01_3)

const waterPatchSide_01_5 = new Entity()
waterPatchSide_01_5.setParent(scene)
waterPatchSide_01_5.addComponentOrReplace(gltfShape_28)
const transform_133 = new Transform({
  position: new Vector3(8, 0, 9),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
waterPatchSide_01_5.addComponentOrReplace(transform_133)
engine.addEntity(waterPatchSide_01_5)

const waterPatchSide_01_6 = new Entity()
waterPatchSide_01_6.setParent(scene)
waterPatchSide_01_6.addComponentOrReplace(gltfShape_28)
const transform_134 = new Transform({
  position: new Vector3(8, 0, 17),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
waterPatchSide_01_6.addComponentOrReplace(transform_134)
engine.addEntity(waterPatchSide_01_6)

const waterPatchCurve_02_3 = new Entity()
waterPatchCurve_02_3.setParent(scene)
waterPatchCurve_02_3.addComponentOrReplace(gltfShape_24)
const transform_135 = new Transform({
  position: new Vector3(8, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchCurve_02_3.addComponentOrReplace(transform_135)
engine.addEntity(waterPatchCurve_02_3)

const waterPatchCurve_01_2 = new Entity()
waterPatchCurve_01_2.setParent(scene)
waterPatchCurve_01_2.addComponentOrReplace(gltfShape_29)
const transform_136 = new Transform({
  position: new Vector3(8, 0, 9),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
waterPatchCurve_01_2.addComponentOrReplace(transform_136)
engine.addEntity(waterPatchCurve_01_2)

const lilyPad_01 = new Entity()
lilyPad_01.setParent(scene)
const gltfShape_30 = new GLTFShape('models/LilyPad_01/LilyPad_01.glb')
lilyPad_01.addComponentOrReplace(gltfShape_30)
const transform_137 = new Transform({
  position: new Vector3(28.5, 0.5, 15.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
lilyPad_01.addComponentOrReplace(transform_137)
engine.addEntity(lilyPad_01)

const lilyPad_01_2 = new Entity()
lilyPad_01_2.setParent(scene)
lilyPad_01_2.addComponentOrReplace(gltfShape_30)
const transform_138 = new Transform({
  position: new Vector3(27, 0.5, 19.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
lilyPad_01_2.addComponentOrReplace(transform_138)
engine.addEntity(lilyPad_01_2)